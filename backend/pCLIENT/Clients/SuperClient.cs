﻿using pCLIENT.Clients;
using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.Text;

namespace pCLIENT.clients
{
    public interface SuperClient
    {
        public abstract Structure ConnectToClient();
        public abstract Structure TranslateToStructure(Super file);
    }
}
