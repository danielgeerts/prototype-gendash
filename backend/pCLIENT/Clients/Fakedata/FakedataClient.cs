﻿using pCLIENT.clients;
using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utf8Json;

namespace pCLIENT.Clients.Fakedata
{
    public class FakedataClient : SuperClient
    {
        public Structure ConnectToClient()
        {
            List<Fakedata> testdata = LoadFakedata();
            return TranslateToStructure(testdata[0]);
        }

        public Structure TranslateToStructure(Super exactObj)
        {
            Fakedata newObj = exactObj as Fakedata;

            Structure newStruct = new Structure();
            newStruct.bronX = "Fakedata: contractHours";
            newStruct.bronY = "Fakedata: workingHours";

            newStruct.x = newObj.contractHours;
            newStruct.y = newObj.workingHours;

            return newStruct;
        }

        private List<Fakedata> LoadFakedata()
        {
            List<Fakedata> items = new List<Fakedata>();
            using (StreamReader r = new StreamReader(@"C:\Users\Daniel\Documents\GenDash\Prototype\prototype-gendash\backend\pCLIENT\Clients\Fakedata\fakedata.json"))
            {
                string json = r.ReadToEnd();
                items = JsonSerializer.Deserialize<List<Fakedata>>(json);
            }
            return items;
        }
    }
}
