﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pCLIENT.Clients.Fakedata
{
    public class Fakedata : Super {
        public string _id { get; set; }
        public int contractHours {get;set;}
        public int workingHours { get; set; }
        public double salary { get; set; }
        public int age { get; set; }
        public string name { get; set; }
        public string favoriteFruit { get; set; }

    }
}
