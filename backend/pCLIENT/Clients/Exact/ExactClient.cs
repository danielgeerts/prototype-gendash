﻿using pCLIENT.clients;
using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Utf8Json;

namespace pCLIENT.Clients.Exact
{
    public class ExactClient : SuperClient
    {
        public Structure ConnectToClient()
        {
            List<Exact> testdata = LoadTestExactData();
            return TranslateToStructure(testdata[0]);
        }

        public Structure TranslateToStructure(Super exactObj)
        {
            Exact newObj = exactObj as Exact;

            Structure newStruct = new Structure();
            newStruct.bronX = "Exact: EV value";
            newStruct.bronY = "Exact: VAT value";

            newStruct.x = newObj.OrderHeader.bdr_ev_val;
            newStruct.y = newObj.OrderHeader.bdr_vat_val;

            return newStruct;
        }

        private List<Exact> LoadTestExactData()
        {
            List<Exact> items = new List<Exact>();
            using (StreamReader r = new StreamReader(@"C:\Users\Daniel\Documents\GenDash\Prototype\prototype-gendash\backend\pCLIENT\Clients\Exact\textdataexact.json"))
            {
                string json = r.ReadToEnd();
                items = JsonSerializer.Deserialize<List<Exact>>(json);
            }
            return items;
        }
    }
}
