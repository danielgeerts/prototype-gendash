﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pCLIENT.Clients.Exact
{
    public class Exact : Super {
        public OrderHeader OrderHeader;
        public List<OrderLines> OrderLines;
        public List<string> DocumentDownloadLinks;
    }

    public class OrderHeader
    {
        public int ID { get; set; }
        public string ordernr { get; set; }
        public string debnr { get; set; }
        public string fakdebnr { get; set; }
        public double bdr_ev_val { get; set; }
        public double bdr_vat_val { get; set; }

    }

    public class OrderLines
    {
        //public OrderLine OrderLine { get; set; }
        //public TextDescription TextDescription { get; set; }
    }

    public class OrderLine
    {
        public int ID { get; set; }
        public int ordernr { get; set; }
    }

    public class TextDescription
    {
        public int ID { get; set; }
        public int ordernr { get; set; }
    }
}
