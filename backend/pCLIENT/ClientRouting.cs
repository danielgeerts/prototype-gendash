﻿using pCLIENT.Clients.Exact;
using pSTRUCTURE;
using Utf8Json;
using System;
using System.Collections.Generic;
using System.IO;
using pCLIENT.Clients.Fakedata;

namespace pCLIENT
{
    public class ClientRouting
    {

        public Structure RouteToAPI(string APIname)
        {
            switch (APIname)
            {
                case "Exact":
                    ExactClient exactClient = new ExactClient();
                    Structure exactResult = exactClient.ConnectToClient();
                    return exactResult;
                case "Fakedata":
                    FakedataClient fakeClient = new FakedataClient();
                    Structure fakeResult = fakeClient.ConnectToClient();
                    return fakeResult;
                default:
                    return SpawnTestData();
            }
        }

        private Structure SpawnTestData()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("     NO SPECIFIC CLIENT MADE     ->      returned structure == null");
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.Black;

            return null;
        }
    }
}
