﻿using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.Text;

namespace pCLIENT
{
    public class ClientProgram
    {
        private User user;

        public ClientProgram(User workingUser) {
            user = workingUser;
        }

        public Structure GetClientData() {
            Console.WriteLine("Try get data from client " + user.APIname);

            ClientRouting tempRouting = new ClientRouting();
            Structure result = tempRouting.RouteToAPI(user.APIname);

            return result;
        }
    }
}
