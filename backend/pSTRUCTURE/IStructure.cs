﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pSTRUCTURE
{
    public interface IStructure
    {

    }

    public class Structure : IStructure
    {
        public String bronX { get; set; }
        public String bronY { get; set; }
        public double x { get; set; }
        public double y { get; set; }

        public double moduleResult { get; set; }
    }
}
