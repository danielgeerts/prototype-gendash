﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pSTRUCTURE
{
    public interface IModule
    {
    }

    public abstract class Module : IModule {

        public virtual Structure ExecuteModule(Structure data)
        {
            return data;
        }
    }
}
