﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pSTRUCTURE
{
    public class User : IUser
    {
        public string token { get; set; }
        public string name { get; set; }
        //public List<Client> clients { get; set; }
        //public List<Module> modules { get; set; }
        public string moduleName { get; set; }
        public string APIname { get; set; }
    }

    public interface IUser
    {
        
    }
}
