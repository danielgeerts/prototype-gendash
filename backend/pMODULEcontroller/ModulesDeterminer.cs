﻿using pMODULES;
using pCUSTOMMODULES.Amazon;
using pCUSTOMMODULES.Facebook;
using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.Text;
using pCUSTOMMODULES.Admin;

namespace pMODULEScontroller
{
    public class ModulesDeterminer
    {

        public Dictionary<string, Module> modules;

        public ModulesDeterminer()
        {
            modules = new Dictionary<string, Module>();
            modules.Add("Admin", new AdminEmployeeEfficiency());
            modules.Add("Amazon", new AmazonEmployeeEfficiency());
            modules.Add("Facebook", new FacebookEmployeeEfficiency());
        }
    }
}
