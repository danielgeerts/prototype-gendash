﻿using pSTRUCTURE;
using System;
using System.Collections.Generic;

namespace pMODULEScontroller
{
    public class ModuleConnection
    {
        public void RouteToModules(User user, Structure data)
        {
            Console.WriteLine();

            Console.WriteLine("Try start of module: " + user.moduleName);

            ModulesDeterminer modules = new ModulesDeterminer();

            foreach (KeyValuePair<string, Module> element in modules.modules)
            {
                if (element.Key.Equals(user.moduleName))
                {
                    Console.WriteLine("FOUND module: {0}", user.moduleName);
                    Console.WriteLine();
                    Console.WriteLine("Do some sum with: {0}: {1} & {2}: {3}", data.bronX, data.x, data.bronY, data.y);

                    Structure result = element.Value.ExecuteModule(data);
                    Console.WriteLine("result: {0}", result.moduleResult);
                    return;
                }
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("NO module found: " + user.moduleName);
            Console.BackgroundColor = ConsoleColor.Black;

        }
    }
}