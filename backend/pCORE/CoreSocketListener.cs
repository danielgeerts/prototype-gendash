﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using pSTRUCTURE;
using Utf8Json;
using pCLIENT;
using pMODULEScontroller;

namespace pCORE
{
    public class CoreSocketListener
    {
        private readonly int port = 11011;
        private readonly IPAddress ip = IPAddress.Parse("127.0.0.1");

        private TcpListener listener;

        public CoreSocketListener() {
            this.listener = new TcpListener(this.ip, this.port);
        }

        public void StartServer()
        {
            this.listener.Start();

            Console.WriteLine("Server running...");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;

                Socket s = this.listener.AcceptSocket();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("New user connected..");
                ThreadPool.QueueUserWorkItem(this.WorkMethod, s);
            }
        }

        private void WorkMethod(object state)
        {
            bool running = true;

            User workingUser = null;

            using (Socket socket = (Socket)state)
            {
                while (running == true)
                {
                    try
                    {
                        byte[] buffer = new byte[1024];

                        int count = socket.Receive(buffer);
                        string jsonString = Encoding.UTF8.GetString(buffer, 0, count);

                        // Try check if message is json
                        try
                        {
                            if (workingUser == null || jsonString.Contains("again"))
                            {
                                try
                                {
                                    if (workingUser == null)
                                    {
                                        workingUser = JsonSerializer.Deserialize<User>(jsonString);
                                        Console.WriteLine("Welcome " + workingUser.name);
                                    }
                                    Console.WriteLine();

                                    // Make console cyan for CLIENT output
                                    Console.ForegroundColor = ConsoleColor.Cyan;
                                    ClientProgram client = new ClientProgram(workingUser);
                                    Structure clientResult = client.GetClientData();
                                    if (clientResult != null)
                                    {
                                        Console.WriteLine("Result OK from client '" + workingUser.APIname + "'; Result type: " + clientResult.GetType());

                                        // Make console yellow for MODULE output
                                        Console.ForegroundColor = ConsoleColor.Yellow;

                                        ModuleConnection moduleConnector = new ModuleConnection();
                                        moduleConnector.RouteToModules(workingUser, clientResult);

                                        Console.WriteLine();
                                        jsonString = "WHAT HAPPEND ->     used API: '" + workingUser.APIname + "', use module: '" + workingUser.moduleName + "'; Calculation: " + clientResult.x + " & " + clientResult.y + " = " + clientResult.moduleResult;
                                        Console.WriteLine(jsonString);
                                        Console.WriteLine();

                                        Console.ForegroundColor = ConsoleColor.White;

                                    } else {
                                        Console.ForegroundColor = ConsoleColor.DarkRed;
                                        Console.WriteLine("clientResult is null -> no data to work with");
                                        Console.WriteLine();
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.ForegroundColor = ConsoleColor.DarkRed;
                                    Console.WriteLine("Something went wrong: " + e);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                            }

                            dynamic jsonOBJ = JsonSerializer.Deserialize<dynamic>(jsonString);
                            //Console.WriteLine("JSON: " + jsonOBJ.Count);
                            Console.WriteLine("         TYPE: " + jsonOBJ.GetType());
                        }
                        // If no JSON received
                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("         << no JSON received >>      ERROR:" + e.Message);
                            Console.WriteLine("         PLAIN TEXT: " + jsonString);

                            if (jsonString.Contains("logout"))
                            {
                                jsonString = "logout";
                                Console.WriteLine("logout... byebye");
                                Console.WriteLine();
                                Console.WriteLine();
                                running = false;
                            }

                            if (jsonString.Contains("close"))
                            {
                                jsonString = "disconnect";
                                Console.WriteLine("disconnecting... byebye");
                                Console.WriteLine();
                                Console.WriteLine();
                                running = false;
                            }
                            
                            Console.ForegroundColor = ConsoleColor.White;
                        }


                        // Send same message back as responce
                        byte[] response = Encoding.ASCII.GetBytes("OK -> " + jsonString);
                        socket.Send(response);
                        Console.WriteLine("         SEND RESPONCE BACK: " + jsonString);
                        Console.WriteLine("====================================");

                    }
                    catch (Exception e) {
                        Console.WriteLine("Sometime went wrong during socket work: " + e.Message + ", " + e.StackTrace);

                        running = false;
                    }
                }
            }

            Console.WriteLine("Closing USER '" + workingUser.name + "' thread, with token: '" + workingUser.token + "'");

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
