﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace pCORE
{
    public class CoreProgram
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("CORE appliction");

            CoreSocketListener s = new CoreSocketListener();
            s.StartServer();

            Console.Write("Press any key to exit...");
            Console.ReadLine();
        }
    }
}
