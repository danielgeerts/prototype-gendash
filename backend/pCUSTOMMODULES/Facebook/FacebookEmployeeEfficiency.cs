﻿using pMODULES;
using System;

namespace pCUSTOMMODULES.Facebook
{
    public class FacebookEmployeeEfficiency : EmployeeEfficiency
    {
        /// <summary>
        /// effiency * 1.2
        /// </summary>
        public override double CalculateEffiency(double employeeHours, double employeebillableHours)
        {
            var baseResult = base.CalculateEffiency(employeeHours, employeebillableHours);

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("  extra facebook: * 0.5f  ");
            Console.ForegroundColor = ConsoleColor.Yellow;


            return baseResult * 0.5f;
        }
    }
}
