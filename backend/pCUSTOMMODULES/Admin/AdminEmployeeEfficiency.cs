﻿using pMODULES;
using System;

namespace pCUSTOMMODULES.Admin
{
    public class AdminEmployeeEfficiency : EmployeeEfficiency
    {
        /// <summary>
        /// effiency * 1.2
        /// </summary>
        public override double CalculateEffiency(double employeeHours, double employeebillableHours)
        {
            var baseResult = base.CalculateEffiency(employeeHours, employeebillableHours);

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("  extra admin: * 0.1f  ");
            Console.ForegroundColor = ConsoleColor.Yellow;

            return baseResult * 0.1f;
        }
    }
}