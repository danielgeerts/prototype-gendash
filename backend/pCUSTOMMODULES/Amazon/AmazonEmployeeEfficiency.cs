﻿using pMODULES;
using System;

namespace pCUSTOMMODULES.Amazon
{
    public class AmazonEmployeeEfficiency : EmployeeEfficiency
    {
        /// <summary>
        /// effiency * 1.2
        /// </summary>
        public override double CalculateEffiency(double employeeHours, double employeebillableHours)
        {
            var baseResult = base.CalculateEffiency(employeeHours, employeebillableHours);

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("  extra amazon: * 1.2f  ");
            Console.ForegroundColor = ConsoleColor.Yellow;


            return baseResult * 1.2f;
        }
    }
}
