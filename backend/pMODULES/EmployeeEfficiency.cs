﻿using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.Text;

namespace pMODULES
{
    public class EmployeeEfficiency : Module
    {
        public override Structure ExecuteModule(Structure data)
        {
            // optioneel
            // data = base.ExecuteModule(data);

            data.moduleResult = CalculateEffiency(data.x, data.y);

            Console.WriteLine(" = ");

            return data;
        }

        public virtual double CalculateEffiency(double employeeHours, double employeebillableHours)
        {
            Console.Write("Calculate: " + employeeHours + "/" + employeebillableHours);

            return employeeHours / employeebillableHours;
        }
    }
}
