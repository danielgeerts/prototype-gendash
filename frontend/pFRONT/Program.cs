﻿using pSTRUCTURE;
using System;

namespace pFRONT
{
    class Program
    {
        static void Main(string[] args)
        {

            ShowWelcomeMessage();

            Connection connection = new Connection();

            connection.CheckIfServerOnline();

            bool loginAgain = true;
            while (loginAgain)
            {
                User verifiedUser = connection.ShowLogin();
                loginAgain = SocketClient.StartClient(verifiedUser);
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
            }

            Console.Write("Press a key to exit...");
            Console.ReadLine();
        }

        private static void ShowWelcomeMessage()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("======================================");
            Console.WriteLine("====== Welcome by our DASHBOARD ======");
            Console.WriteLine("======================================");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
