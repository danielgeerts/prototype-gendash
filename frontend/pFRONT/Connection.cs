﻿using pAUTH;
using pSTRUCTURE;
using System;
using System.Collections.Generic;
using System.Text;

namespace pFRONT
{
    public class Connection
    {
        public void CheckIfServerOnline()
        {
            if (true)
            {
                Console.WriteLine("SYSTEMS are online !");
            }
            else
            {
                Console.WriteLine("AUTH of CORE offline ! Push any key to try again.");
                Console.ReadLine();
                CheckIfServerOnline();
            }
            Console.WriteLine();
        }

        public User ShowLogin()
        {
            Console.Write("Enter username: ");
            string user = Console.ReadLine();

            Console.Write("Enter password: ");
            string psword = Console.ReadLine();

            // check if user existes and AUTHENTICATE
            User result = Authenticator.TryLogin(user, psword);
            if (result != null)
            {
                Console.WriteLine("Succes login");
                return result;
            }

            Console.WriteLine("Login failed ! Try again");
            return ShowLogin();
        }
    }
}
