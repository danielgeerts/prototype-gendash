﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using pSTRUCTURE;
using Utf8Json;

namespace pFRONT
{
    class SocketClient
    {
        private static bool running = true;

        public static bool StartClient(User user)
        {
            running = true;
            bool mayLoginAgain = true;
            

            try
            {
                using (TcpClient tcpclnt = new TcpClient())
                {
                    Console.WriteLine("Connecting...");
                    tcpclnt.Connect("127.0.0.1", 11011);
                    Console.WriteLine("Connected !");

                    using (var stream = tcpclnt.GetStream())
                    {
                        string result = JsonSerializer.ToJsonString(user);
                        string userResponce = SendMessage(tcpclnt, stream, result);
                        handleResponce(userResponce);

                        while (running)
                        {
                            string responce = SendMessage(tcpclnt, stream);
                            mayLoginAgain = handleResponce(responce);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... Kan niet verbinden");
                Console.WriteLine(e.Message);
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Press any key: to login again...");
                Console.ReadLine();

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();

                return true;
            }

            return mayLoginAgain;
        }

        public static bool handleResponce(string responce) {
            if (responce.ToLower().Trim().Contains("disconnect"))
            {
                running = false;
            }

            Console.WriteLine("====================================");

            if (responce.ToLower().Trim().Contains("logout"))
            {
                running = false;
                return true;
            }

            return false;
        }

        public static string SendMessage(TcpClient tcp, NetworkStream stream)
        {
            Console.Write("What to send? ");
            string toSend = Console.ReadLine();
           
            return SendMessage(tcp, stream, toSend);
        }

        private static string SendMessage(TcpClient tcp, NetworkStream stream, string msgToSend) {
            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] bytesToSend = asen.GetBytes(msgToSend);
            stream.Write(bytesToSend, 0, bytesToSend.Length);

            byte[] bytesToRead = new byte[tcp.ReceiveBufferSize];
            int bytesRead = stream.Read(bytesToRead, 0, tcp.ReceiveBufferSize);

            string received =  Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Received : " + received);
            Console.ForegroundColor = ConsoleColor.Gray;
            return received;
        }
    }
}
