﻿using pSTRUCTURE;
using System;
using System.Collections.Generic;

namespace pAUTH
{
    public static class Authenticator
    {
        private static List<User> users = null;

        public static User TryLogin(string username, string password) {
            GenerateFakeData();

            Console.WriteLine("        <<< AUTH >>> : " + username);

            User loggedInUser = new User();

            foreach (User u in users) {
                if (u.name.Equals(username)) {
                    loggedInUser = u;
                }

                if ((username.Equals(u.name) && password.Equals("123")))
                {
                    return loggedInUser;
                }
            }

            return null;
        }

        public static bool IsLoggedin(string name) {
            foreach (User u in users)
            {
                if (u.name.Equals(name)) {
                    return true;
                }
            }
            return false;
        }

        private static void GenerateFakeData() {
            if (users == null)
            {
                users = new List<User>();

                users.Add(new User {
                    name = "admin",
                    token = "Bearer token_for_admin",
                    //clients = new List<Client> { new Client("Exact") },
                    //modules = new List<Module> { new Module("Amazon") }
                    APIname = "Exact",
                    moduleName = "Admin"
                });

                users.Add(new User
                {
                    name = "empty",
                    token = "Bearer token_for_empty"
                });

                users.Add(new User {
                    name = "facebook",
                    token = "Bearer token_for_facebook",
                    //clients = new List<Client> { new Client("Facebook") },
                    //modules = new List<Module> { new Module("Facebook") }
                    APIname = "Fakedata",
                    moduleName = "Facebook"
                });

                users.Add(new User
                {
                    name = "amazon",
                    token = "Bearer token_for_amazon",
                    APIname = "Exact",
                    moduleName = "Amazon"
                });
            }
        }
    }
}
